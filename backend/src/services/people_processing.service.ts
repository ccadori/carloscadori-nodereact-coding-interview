import people_data from '../data/people_data.json';
import { ISearchPeopleQuery } from '../dtos/searchPeopleQuery.dto';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll() {
        return people_data;
    }

    search(query: ISearchPeopleQuery) {
        if (!query.title && !query.gender && !query.company) {
            return this.paginate(people_data, query.offset, query.limit);
        }

        const filteredPeople = people_data.filter((people) => {
            let matched: boolean = false;
            if (query.title) {
                if (
                    people.title &&
                    people.title.toLowerCase().includes(query.title.toLowerCase())
                ) {
                    matched = true;
                }
            }
            if (query.company) {
                if (
                    people.company.toLowerCase().includes(query.company.toLowerCase())
                ) {
                    matched = true;
                }
            }
            if (query.gender) {
                if (
                    people.gender.toLowerCase().includes(query.gender.toLowerCase())
                ) {
                    matched = true;
                }
            }
            return matched;
        });

        return this.paginate(filteredPeople, query.offset, query.limit);
    }

    private paginate(data: Array<object>, offset: number, limit: number) {
        const startAt = offset * limit;
        const endsAt = startAt + limit;

        return data.slice(startAt, endsAt);
    }
}

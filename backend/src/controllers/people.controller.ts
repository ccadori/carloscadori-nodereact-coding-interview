import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param,
    QueryParams,
} from 'routing-controllers';
import { IsNumber, IsString, IsOptional } from 'class-validator'
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

class SearchPeopleQuery {
    @IsNumber()
    limit: number;
    @IsNumber()
    offset: number;
    @IsString()
    @IsOptional()
    company?: string;
    @IsOptional()
    @IsString()
    gender?: string;
    @IsOptional()
    @IsString()
    title?: string;
}

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople() {
        const people = peopleProcessing.getAll();

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/search')
    searchPeople(@QueryParams() query: SearchPeopleQuery) {
        const result = peopleProcessing.search(query);
        return {
            data: result
        }
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}

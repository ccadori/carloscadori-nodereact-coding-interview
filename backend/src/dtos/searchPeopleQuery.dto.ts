export interface ISearchPeopleQuery {
  title?: string;
  gender?: string;
  company?: string;
  offset: number;
  limit: number;
}